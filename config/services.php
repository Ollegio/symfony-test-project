<?php

use App\Serializer\ValidationExceptionNormalizer;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

use function Symfony\Component\DependencyInjection\Loader\Configurator\param;

return static function (ContainerConfigurator $configurator) {
    $services = $configurator->services();
    $services->defaults()
        ->autowire(true)
        ->autoconfigure(true);

    $services->load('App\\', '../src/*')
        ->exclude(['../src/DependencyInjection/', '../src/Entity/', '../src/Kernel.php', '../src/Tests/']);

    $services->load('App\\Controller\\', '../src/Controller/')
        ->tag('controller.service_arguments');

    $services->set(ValidationExceptionNormalizer::class, ValidationExceptionNormalizer::class)
        ->args([param('kernel.debug')])
        ->tag('serializer.normalizer', ['priority' => -890]);
};
