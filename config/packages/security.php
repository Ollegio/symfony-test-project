<?php

use App\Entity\Enums\RoleEnum;
use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationFailureHandler;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;
use Symfony\Component\Security\Core\Authorization\Voter\AuthenticatedVoter;
use Symfony\Config\SecurityConfig;

return static function (SecurityConfig $security) {
    $security->enableAuthenticatorManager(true);

    $security->encoder(User::class)
        ->algorithm('auto');

    $security->provider('app_user_provider')
        ->entity()
        ->class(User::class)
        ->property('username');

    $security->firewall('dev')
        ->pattern('^/(_(profiler|wdt)|css|images|js)/')
        ->security(false);

    $security->firewall('login')
        ->pattern('^/api/auth')
        ->stateless(true)
        ->jsonLogin()
        ->checkPath('/api/auth')
        ->successHandler(AuthenticationSuccessHandler::class)
        ->failureHandler(AuthenticationFailureHandler::class);

    $security->firewall('api')
        ->pattern('^/api')
        ->stateless(true)
        ->jwt();

    $security->accessControl()->path('^/api/auth')->roles([AuthenticatedVoter::IS_AUTHENTICATED_ANONYMOUSLY]);
    $security->accessControl()->path('^/api')->roles([RoleEnum::USER]);
};
