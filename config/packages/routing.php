<?php

use Symfony\Config\FrameworkConfig;

return static function (FrameworkConfig $framework) {
    $router = $framework->router();
    $router->utf8(true);
};
