<?php

use Symfony\Config\MonologConfig;
use Psr\Log\LogLevel;

return static function (MonologConfig $monolog) {
    $monolog->handler('main')
        ->type('stream')
        ->path('%kernel.logs_dir%/%kernel.environment%.log')
        ->actionLevel(LogLevel::DEBUG)
        ->channels()->elements(['!event']);

    $monolog->handler('console')
        ->type('console')
        ->processPsr3Messages(false)
        ->channels()->elements(['!event', '!doctrine', '!console']);
};
