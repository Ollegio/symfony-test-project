<?php

use App\Doctrine\Filter\SoftDeleteFilter;
use Symfony\Config\DoctrineConfig;

use function Symfony\Component\DependencyInjection\Loader\Configurator\env;
use function Symfony\Component\DependencyInjection\Loader\Configurator\param;

return static function (DoctrineConfig $doctrine) {
    $doctrine->dbal()
        ->connection('default')
        ->url(env('resolve:DATABASE_URL'));

    $doctrine->orm()
        ->autoGenerateProxyClasses(true);

    $em = $doctrine->orm()->entityManager('default');
    $em->namingStrategy('doctrine.orm.naming_strategy.underscore_number_aware')
        ->autoMapping(true)
        ->mapping('App')
        ->isBundle(false)
        ->type('attribute')
        ->dir(param('kernel.project_dir') . '/src/Entity')
        ->prefix('App\Entity');

    $em->filter(SoftDeleteFilter::FILTER_NAME)->class(SoftDeleteFilter::class)->enabled(true);
};
