<?php

use Psr\Log\LogLevel;
use Symfony\Config\MonologConfig;

return static function (MonologConfig $monolog) {
    $main = $monolog->handler('main');
    $main->type('fingers_crossed')
        ->actionLevel(LogLevel::ERROR)
        ->handler('nested')
        ->channels()->elements(['!event']);
    $main->excludedHttpCode()->code(404);
    $main->excludedHttpCode()->code(405);

    $nested = $monolog->handler('nested');
    $nested->type('stream')
        ->path('%kernel.logs_dir%/%kernel.environment%.log')
        ->level(LogLevel::DEBUG);
};
