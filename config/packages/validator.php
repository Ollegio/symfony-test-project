<?php

use Symfony\Config\FrameworkConfig;
use Symfony\Component\Validator\Constraints\Email;

return static function (FrameworkConfig $framework) {
    $framework->validation()
        ->emailValidationMode(Email::VALIDATION_MODE_HTML5);
};
