<?php

use Symfony\Config\DoctrineMigrationsConfig;

return static function (DoctrineMigrationsConfig $migrations) {
    $migrations->migrationsPath('DoctrineMigrations', '%kernel.project_dir%/migrations');
};
