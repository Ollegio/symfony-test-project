<?php

use Symfony\Config\LexikJwtAuthenticationConfig;

use function Symfony\Component\DependencyInjection\Loader\Configurator\env;

return static function (LexikJwtAuthenticationConfig $config) {
    $config
        ->secretKey(env('resolve:JWT_SECRET_KEY'))
        ->publicKey(env('resolve:JWT_PUBLIC_KEY'))
        ->passPhrase(env('JWT_PASSPHRASE'));
};
