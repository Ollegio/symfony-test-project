<?php

use Symfony\Config\SensioFrameworkExtraConfig;

return static function (SensioFrameworkExtraConfig $sensio) {
    $sensio->router()->annotations(false);
};
