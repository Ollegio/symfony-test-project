<?php

use Symfony\Config\MonologConfig;
use Psr\Log\LogLevel;

return static function (MonologConfig $monolog) {
    $main = $monolog->handler('main');
    $main->type('fingers_crossed')
        ->actionLevel(LogLevel::ERROR)
        ->handler('nested')
        ->bufferSize(50);
    $main->excludedHttpCode()->code(404);
    $main->excludedHttpCode()->code(405);

    $monolog->handler('nested')
        ->type('stream')
        ->path('php://stderr')
        ->level(LogLevel::DEBUG)
        ->formatter('monolog.formatter.json');

    $monolog->handler('console')
        ->type('console')
        ->processPsr3Messages(false)
        ->channels()->elements(['!event', '!doctrine']);
};
