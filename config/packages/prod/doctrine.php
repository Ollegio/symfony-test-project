<?php

use Symfony\Config\DoctrineConfig;
use Symfony\Config\FrameworkConfig;

return static function(DoctrineConfig $doctrine, FrameworkConfig $framework) {
    $doctrine->orm()
        ->autoGenerateProxyClasses(false);
    $em = $doctrine->orm()->entityManager('default');
    $em->metadataCacheDriver()
        ->type('pool')
        ->pool('doctrine.system_cache_pool');
    $em->queryCacheDriver()
        ->type('pool')
        ->pool('doctrine.system_cache_pool');
    $em->resultCacheDriver()
        ->type('pool')
        ->pool('doctrine.result_cache_pool');

    $framework->cache()
        ->pool('doctrine.result_cache_pool')->adapters(['cache.app']);
    $framework->cache()
        ->pool('doctrine.system_cache_pool')->adapters(['cache.system']);
};
