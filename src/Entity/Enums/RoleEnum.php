<?php

namespace App\Entity\Enums;

class RoleEnum
{
    public const USER = 'ROLE_USER';
    public const ADMIN = 'ROLE_ADMIN';
}
