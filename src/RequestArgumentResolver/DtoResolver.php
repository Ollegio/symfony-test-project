<?php

namespace App\RequestArgumentResolver;

use App\Data\Dto;
use App\Service\ValidatorService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class DtoResolver implements ParamConverterInterface
{
    public function __construct(private NormalizerInterface $normalizer, private ValidatorService $validator)
    {
    }

    public function supports(ParamConverter $configuration): bool
    {
        return is_subclass_of($configuration->getClass(), Dto::class);
    }

    public function apply(Request $request, ParamConverter $argument)
    {
        $data = $request->toArray();
        $type = $argument->getClass();
        $dto = $type::fromArray($data);
        $this->validator->validate($dto);
        $request->attributes->set($argument->getName(), $dto);

        return true;
    }
}
