<?php

namespace App\Controller;

use App\Data\CreateUserDto;
use App\Data\UpdateUserDto;
use App\Entity\Enums\RoleEnum;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Service\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

#[Route('/api/users')]
class UserController extends AbstractController
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    #[Route('/me')]
    public function me(): JsonResponse
    {
        return $this->json($this->getUser());
    }

    #[Route('', methods: ['GET'])]
    public function index(Request $request): JsonResponse
    {
        $filters = $request->query->get('filter') ?? [];
        $orderBy = $request->query->get('sort') ?? [];

        $users = $this->userService->getUsers($filters, $orderBy);
        return $this->json($users);
    }

    #[Route('', methods: ['POST'])]
    public function create(CreateUserDto $data): JsonResponse
    {
        $user = $this->userService->createUser($data);

        return $this->json($user, Response::HTTP_CREATED);
    }

    #[Route('/{id}', methods: ['GET'])]
    public function show(User $user): JsonResponse
    {
        return $this->json($user);
    }

    #[Route('/{id}', methods: ['PUT'])]
    public function update(User $user, UpdateUserDto $data): JsonResponse
    {
        $user = $this->userService->updateUser($user, $data);

        return $this->json($user);
    }

    #[Route('/{id}', methods: ['DELETE'])]
    public function delete(User $user): JsonResponse
    {
        $this->userService->delete($user);

        return $this->json('', Response::HTTP_NO_CONTENT);
    }
}
