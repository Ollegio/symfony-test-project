<?php


namespace App\Doctrine\Filter;


use App\Entity\Traits\SoftDeletable;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class SoftDeleteFilter extends SQLFilter
{
    public const FILTER_NAME = 'softDelete';

    /**
     * @inheritDoc
     */
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias): string
    {
        $traits = $targetEntity->reflClass->getTraits();
        if (!array_key_exists(SoftDeletable::class, $traits)) {
            return '';
        }

        return "$targetTableAlias.deleted_at is null";
    }
}
