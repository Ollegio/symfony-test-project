<?php

namespace App\Data;

class Dto
{
    public static function fromArray(array $data): static
    {
        $data = array_intersect_key($data, get_class_vars(static::class));
        return new static(...$data);
    }
}
