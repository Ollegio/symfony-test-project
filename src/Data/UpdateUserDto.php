<?php

namespace App\Data;

use Symfony\Component\Validator\Constraints as Assert;

class UpdateUserDto extends Dto
{
    public function __construct(
        #[Assert\Length(min: 4, max: 255)]
        public $username = null,
    ) {
    }
}
