<?php

namespace App\Data;

use Symfony\Component\Validator\Constraints as Assert;

class CreateUserDto extends Dto
{
    public function __construct(
        #[Assert\NotBlank, Assert\Length(min: 4, max: 255)]
        public $username = null,

        #[Assert\NotBlank, Assert\Length(min: 6, max: 255)]
        public $password = null,
    ) {
    }
}
