<?php


namespace App\Service;


use App\Exception\ValidationException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidatorService
{
    public function __construct(
        private ValidatorInterface $validator
    )
    {
    }

    /**
     * @param $data
     * @param null|Constraint|Constraint[] $constraints
     */
    public function validate($data, $constraints = null)
    {
        $errors = $this->validator->validate($data, $constraints);

        if ($errors->count() > 0) {
            throw new ValidationException($errors);
        }
    }
}
