<?php

namespace App\Service;

use App\Data\CreateUserDto;
use App\Data\UpdateUserDto;
use App\Entity\User;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class UserService
{

    public function __construct(
        private EntityManagerInterface $em,
        private UserRepository $userRepository,
        private DenormalizerInterface $normalizer,
        private UserPasswordHasherInterface $passwordHasher,
        private ValidatorService $validator,
    ) {
    }

    public function getUsers(array $filters, array $orderBy): array
    {
        return $this->userRepository->findBy($filters, $orderBy);
    }

    public function createUser(CreateUserDto $data): User
    {
        /** @var User $user */
        $user = $this->normalizer->denormalize($data, User::class);
        $user->setPassword($this->passwordHasher->hashPassword($user, $data->password));
        $this->validator->validate($user);
        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    public function updateUser(User $user, UpdateUserDto $data): User
    {
        $this->normalizer->denormalize($data, User::class, context: [AbstractNormalizer::OBJECT_TO_POPULATE => $user]);
        $this->validator->validate($user);
        $this->em->flush();

        return $user;
    }

    public function delete(User $user): void
    {
        $user->setDeletedAt(new DateTime());

        $this->em->flush();
    }
}
