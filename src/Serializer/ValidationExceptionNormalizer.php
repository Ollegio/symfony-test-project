<?php

namespace App\Serializer;

use App\Exception\ValidationException;
use Symfony\Component\ErrorHandler\Exception\FlattenException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ProblemNormalizer;

class ValidationExceptionNormalizer extends ProblemNormalizer
{
    /**
     * @param FlattenException $object
     * @param string|null $format
     * @param array $context
     * @return array
     * @throws ExceptionInterface
     */
    public function normalize($object, string $format = null, array $context = [])
    {
        $data = parent::normalize($object);

        $originalException = $context['exception'] ?? null;

        if ($originalException instanceof ValidationException) {
            foreach ($originalException->getErrors() as $error) {
                $data['errors'][$error->getPropertyPath()][] = $error->getMessage();
            }
        }

        return $data;
    }

    public function supportsNormalization($data, string $format = null): bool
    {
        return $data instanceof FlattenException;
    }
}
